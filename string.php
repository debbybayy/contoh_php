<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Belajar String</title>
</head>
<body>
    <h2>Contoh Soal</h2>
    <?php
        echo "<h3>Contoh Soal 1</h3>";
        $kalimat_1 = "Hello World";
        echo "Kalimat Pertama : ". $kalimat_1 ."<br>";
        echo "Panjang String : ". strlen($kalimat_1). "<br>";
        echo "Jumlah Kata : ". str_word_count($kalimat_1). "<br><br>";

        echo "<h3>Contoh Soal 2</h3>";
        $kalimat_2 = "Nama Saya Bayu";
        echo "Kalimat Kedua : ". $kalimat_1. "<br>";
        echo "Kata Pertama : ". substr($kalimat_2,0,4)."<br>";
        echo "Kata Kedua : ". substr($kalimat_2,5,4)."<br>";
        echo "Kata Ketiga : ". substr($kalimat_2,10,4)."<br>";

        echo "<h3>Contoh Soal 2</h3>";
        $kalimat_3 =    "Selamat Pagi";
        echo "Kalimat Ketiga : ". $kalimat_3. "<br>";
        echo "Ganti Kalimat Ketiga : ". str_replace("Pagi","Malam",$kalimat_3);
    ?>
</body>
</html>